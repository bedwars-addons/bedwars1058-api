This only contains a maven repo for BedWars1058 Plugin.


```xml
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/14122684/packages/maven</url>
  </repository>
</repositories>

<dependencies>
    <dependency>
        <groupId>com.andrei1058.bedwars</groupId>
        <artifactId>bedwars-api</artifactId>
        <version>1.8-SNAPSHOT</version>
        <scope>VERSION-HERE</scope>
    </dependency>
</dependencies>

```